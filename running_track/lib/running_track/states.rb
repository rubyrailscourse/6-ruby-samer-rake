require 'workflow'
module RunningTrack
  class Track

    include Workflow

    module States

      def self.included entity
        entity.workflow do
          state(:new) do
            event :set_good, transitions_to: :good
            event :set_normal, transitions_to: :normal
            event :set_bad, transitions_to: :bad
          end
          state(:good) do
            event :set_new, transitions_to: :new
            event :set_normal, transitions_to: :normal
            event :set_bad, transitions_to: :bad
          end
          state(:normal) do
            event :set_good, transitions_to: :good
            event :set_new, transitions_to: :new
            event :set_bad, transitions_to: :bad
          end
          state(:bad) do
            event :set_good, transitions_to: :good
            event :set_normal, transitions_to: :normal
            event :set_new, transitions_to: :new
          end
        end
      end
    end
  end
end
