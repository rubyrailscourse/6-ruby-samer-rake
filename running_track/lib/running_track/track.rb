require_relative 'states'
module RunningTrack
  class Track
    include States

    attr_accessor :id, :district, :address, :phone, :wifi

    def initialize(record, i)
      @id = i
      @district = record[0].join
      @address = record[1]
      @phone = record[2]
      @wifi = record[3]
    end

    def to_s
      "Id: #{@id}, district: #{@district}, address: #{@address}, phone: #{@phone}, wifi: #{@wifi}"
    end
  end
end
