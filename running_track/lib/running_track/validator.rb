module RunningTrack
  module Validator
    class DataNilError < StandardError
    end

    def valid? data
      check_nil_data! data
    rescue DataNilError => error
      puts error
    ensure
      return error.nil?
    end

    private
    def check_nil_data! data
      raise DataNilError, "We can not receive data from the server!" if data.nil?
    end
  end
end
