require_relative 'track'
require_relative 'validator'
require_relative 'data'

module RunningTrack

  class TrackCollection
    include Enumerable
    include Validator

    HEADINGS = ["District", "Address", "HelpPhone", "ObjectHasWifi"]

    attr_accessor :rows

    def initialize data
      @rows = prepare data if valid? data
    end

    def each &block
      @rows.each &block
    end

    private

    def prepare data
      JSON.parse(data.read).map!.with_index do |row, i|
        record = row['Cells'].slice(*HEADINGS).values
        Track.new(record, i)
      end
    end
  end
end
