module RunningTrack
  module Data

    DATA_URL = "http://api.data.mos.ru/v1/datasets/899/rows"

    def self.import
      Timeout::timeout(5) {open DATA_URL}
    rescue Timeout::Error
      puts "Query-time error!"
    end
  end
end
