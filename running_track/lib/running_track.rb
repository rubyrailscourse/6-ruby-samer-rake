require_relative 'running_track/data'
require_relative 'running_track/track_collection'
require_relative 'running_track/track'
require_relative 'running_track/states'
require 'active_support/all'
require 'open-uri'
require 'timeout'
require 'yaml'
require 'json'

module RunningTrack

  TRACKS_FILE_PATH = "data/file.yml"

  def self.create
    data = Data.import
    @collection = TrackCollection.new data
    @collection = @collection.rows
  end

  def self.load
    @collection = YAML.load_file(TRACKS_FILE_PATH)
  end

  def self.print
    @collection.each {|track| puts track}
  end

  def self.select num
    @collection.find {|track| track.id == num}
  end

  def self.find(name, value)
    @collection.find {|track| track.send(name) == value}
  end

  def self.random count = 1
    @collection.sample count
  end

  def self.save
    output = YAML.dump @collection
    File.write(TRACKS_FILE_PATH, output)
  end
end

# RunningTrack.create
RunningTrack.load
# RunningTrack.print
# puts RunningTrack.find("address", "улица Полбина, дом 44")
# puts RunningTrack.random 2
track = RunningTrack.select(6)
puts track
puts track.current_state
track.set_bad!
puts track.current_state
RunningTrack.save
RunningTrack.load
track = RunningTrack.select(6)
puts track.current_state
track.set_new!
puts track.current_state
puts track.new?
