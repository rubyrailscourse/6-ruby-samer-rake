require 'active_support/all'
# Numeric:
# Метод, возвращающий объект класса ActiveSupport::Duration,
# представляющий n часов, n минут, n секунд
n = 10
n.hour
n.minutes
n.seconds
# Метод, возвращающий экземпляр класса Time, со значением времени,
# спустя n секунд от текущего момента времени
20.days.since
# Метод, возвращающий экземпляр класса Time, со значением времени,
# n секунд тому назад от текущего момента
20.days.ago

# String:
# Метод, возвращающий константу с именем, что и значение строки
'Class'.constantize
# Метод, возвращающий множественное число для значения строки
'post'.pluralize
# Метод, возвращающий строку без подстрок, совпавших по шаблону,
# который передан в метод в качестве аргумента
str = "foo bar test"
str.remove(" test", /bar/)
# Метод, проверяющий, стостоит ли строка лишь из пробельных символов
'   '.blank?
# Метод, заменяющий символ '_' на '-'
'puni_puni'.dasherize

# ActiveSupport::Configurable
# Метод для доступа к упорядоченному хэшу, хранящему конфигурацию
# (пользовательские настройки) для класса
class User
  include ActiveSupport::Configurable
  config.admin = false
end
p User.config
p User.config.admin
